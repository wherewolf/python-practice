#This program demonstrates for loops in lists in python. 

supplies = []
while True:
	print('Enter an office supply.')
	print('If finished, enter nothin.')
	supply = raw_input()
	if supply == '':
		break
	supplies = supplies + [supply]

# supplies = ['pens', 'staplers', 'flame-throwers', 'binders']
for i in range(len(supplies)):
	print('Index ' + str(i) + ' in supplies is: ' + supplies[i])