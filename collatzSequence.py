#This is a program that takes an integer from the user and then runs through the collatz algorithm
#
#From Wikipedia: 
#	"The Collatz sequence, also called the Hailstone sequence, is a sequence of numbers relevant 
#	to the Collatz conjecture, which theorizes that any number using this algorithm will eventually 
#	be reduced to 1. The conjecture's truth is supported by calculations, but it hasn't yet been 
#	proved that no number can indefinitely stay above 1.""

def collatz(number):
	if number % 2 == 0:
		print(str(number // 2))
		return number // 2
	else:
		print(3 * number + 1)
		return 3 * number + 1

print ""
print "Please type an integer greater than 1."

try:
	n = int(input()) 
	while n != 1:
		n = collatz(n)
except:
	print "Please type an integer and not a float or a string."

print ""