#this program demonstrates how to pass an argument into a function in python

def hello(name):
	print 'Hello ' + name

hello('Alice')
hello('Bob')